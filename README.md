eSport-One
==========

"First" engine sport v2.2

--------------------------------------------------------------------

eSport v2.2 [alpha] build 886:

   - CutTheLink [ beta v1.1 ] ok 
   - CPDF [ alpha v0.1 ]      no
   - AutoInclude_once 2.0     ok
   - Counter[cy/pr/alexa]     ok
   - Banner [check+]	      no
   - админка [не разрабатыв.] no
   - папка для сессии         ok
   - папка для логов          ok
   - папка шрифтов            ok
   - модули:		      	  no
	- Графический модуль 	  ok
	- fpdf		      		  ok
	- каптча	      		  ok
	- seo 		      		  no
	- userbar	      		  no
   - tpl автоматизация	      ok
   - CTL-русская локализ.ссыл.ok
   - mail+feedback	      	  ok
   - decode		      		  ok

[08.12.2012|01:31|eSport v2.2D]

------------------------------------------------------------------------
