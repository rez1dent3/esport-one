<?php

/**
 * @author REZ1DENT3
 * @copyright 2012
**/

define ('developer',     TRUE);
define ('Access',		 FALSE);
define ('eSportSet',     TRUE);
define ('eSport',		 '/engine/');
define ('eSportData',    eSport.'data/');
define ('eSportModule',  eSport.'module/');
define ('eSportLog',     eSport.'log/');
define ('eSportClasses', eSport.'classes/');
define ('USER_AGENT',    $_SERVER['HTTP_USER_AGENT']);
define ('eSportRoot',    "http://".$_SERVER['SERVER_NAME']."/");
define ('eSportSetTpl',	 "HTML5"); // creat - mysql_query() - for - users - in - set - tpl
define ('eSportTpl',	 eSportRoot."templates/".eSportSetTpl."/");
define ('eSportVersion', "2.2");
define ('CHROME',        (int)!stristr(USER_AGENT, 'CHROME'));