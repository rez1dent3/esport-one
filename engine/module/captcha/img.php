<?php

/**
 * @author REZ1DENT3
 * @copyright 2012
**/
 
session_start();

if(isset($_SESSION['cap'])){
    unset($_SESSION['cap']);
}

class capt {
    var $caption = array(
        'length'=>4,
        'red'=>null,
        'green'=>null,
        'blue'=>null,
        'random'=>null,
        'width'=>125,
        'height'=>60,
        'font-size-space'=>20
    );
    
    function rgb(){
    
        $name_col = array('red','green','blue');
    
        
        for($i=0;$i<count($name_col);$i++){
            for($j=0;$j<($this->caption['length']);$j++){
                $rnd = rand(0,255);
                $color[$name_col[$i]][$j] = (strlen($rnd) == 1)?'00'.$rnd:((strlen($rnd) == 2)?'0'.$rnd:$rnd);
            }
        }

        $this->caption['red'] = $color['red'];
        $this->caption['green'] = $color['green'];
        $this->caption['blue'] = $color['blue'];
        
    }
    
    function rnd(){

        for($i=0;$i<($this->caption['length']);$i++){
            $rnd[$i] = (rand(0,1)==1)?chr(rand(ord("A"),ord("Z"))):rand(0,9);           
        }
        
        $this->caption['random'] = $rnd;
        
    }
    
    function creat_image() {
        
        $letterWidth = intval((0.7*$this->caption['width'])/$this->caption['length']);
        $image = imagecreatetruecolor($this->caption['width'],$this->caption['height']);
        $fon = imagecolorallocate($image, $this->caption['red'][rand(0,count($this->caption['red'])-1)], $this->caption['green'][rand(0,count($this->caption['green'])-1)], $this->caption['blue'][rand(0,count($this->caption['blue'])-1)]);
        imagefill($image, 0, 0, $fon);
        
        for($j=0; $j<($this->caption['width']); $j++){
          for($i=0; $i<($this->caption['height']*$this->caption['width'])/1500; $i++) {
            $col = imagecolorallocatealpha(
                     $image,
                     $this->caption['green'][rand(0,$this->caption['length']-1)],
                     $this->caption['red'][rand(0,$this->caption['length']-1)],
                     $this->caption['blue'][rand(0,$this->caption['length']-1)],
                     rand(10,20)); 
            imagesetpixel($image, rand(0,$this->caption['width']), rand(0,$this->caption['height']), $col);
          }
        }
        
        for($i=0;$i<($this->caption['length']);$i++){
            $col = imagecolorallocatealpha($image,$this->caption['green'][rand(0,$this->caption['length']-1)],$this->caption['red'][rand(0,$this->caption['length']-1)],$this->caption['blue'][rand(0,$this->caption['length']-1)],rand(10,30)); 
            ImageTTFtext($image, 26, rand(-15,20), $this->caption['font-size-space'], rand(25,30), $col, "League_Gothic-webfont.ttf", $this->caption['random'][$i]);
			ImageTTFtext($image, 10, 0, 10, 35, $col, "trebucit.ttf", "www.gs-pc.ru");
            ImageTTFtext($image, 10, 0, 10, 45, $col, "trebucit.ttf", "8gs.ru");
            $this->caption['font-size-space'] += 22;
            $_SESSION['cap'] .= $this->caption['random'][$i];
        }
        
        $_SESSION['cap'] = strtolower($_SESSION['cap']);
                
        header('Content-type: image/png');
        imagepng($image);
        imagedestroy($image);
        
    }
    
    function compile(){
        $this->rgb();
        $this->rnd();
        $this->creat_image();
    }
    
}

$captcha = new capt;
$captcha->compile();

?>
