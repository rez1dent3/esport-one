<?php

/**
 * @author REZ1DENT3
 * @copyright 2012
**/

class tpl {
    
    var $MatchTpl,$ReplaceTpl,$main;
    
    function SetMainTpl($main){
    
		$this->main=(CHROME)?iconv('utf-8','windows-1251',$main):$main;
    
	}
    
    function SetPointer($Match,$Replace){
        
        if (CHROME) {
            $Match = iconv('utf-8','windows-1251',$Match);
            $Replace=iconv('utf-8','windows-1251',$Replace);
        }
        
        $this->MatchTpl[]=$Match;
        $this->ReplaceTpl[]=$Replace;
    
	}
    
}